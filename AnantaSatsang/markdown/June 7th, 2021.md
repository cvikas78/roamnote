- But freedom is actually not a state. 

It is freedom, including freedom from states. 

It is freedom from identification with our mind, it is freedom from identification with events that happen around us, and it is freedom from identification with our own emotions. 

So if our freedom was dependent on a particular transient state, then we would not value that freedom so much. Isn’t it?

[[Suggested: Freedom]] [[States of Body-Mind]][[Desire for Spiritual States]]

Source: Book - Are You Aware Now, Pg. 13 
