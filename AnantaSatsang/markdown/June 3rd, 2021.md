- Because we have a construct of what freedom 'looks like', then when life shows us that life cannot be in service to any construct like that and usually pushes the button the other way, then what happens is that the mind uses that and tries to convince you that you are not really as free as you think you are.

Source: 
Editor: [[Ananta]]
[[Suggested: Freedom]] [[Suggested: Spiritual Expection]][[Mind Tricks]] [[Untagged]] 
- 
- [[Approved Tags]]
- ![](https://firebasestorage.googleapis.com/v0/b/firescript-577a2.appspot.com/o/imgs%2Fapp%2FAnantaSatsang%2FKSGzARiTQN.jpeg?alt=media&token=97623a01-861c-45ca-972d-fc831af795b3)
- Quote Text: 
The recognition
