- This idea that ‘I’ know what states should come, I know better than consciousness what should arise for me is the precursor to becoming a guard to states that may arise, like peace, joy, etc. 

**If real peace, real joy, is present, it does not need to be guarded.** **Only a conceptual idea or a thought or notion of ‘peace’ or ‘joy’, or any other state needs guarding.**

Source: May 24th Satsang Broadcast

[[Experience Chasing]] [[Conceptual Knowledge]] [[States of Body-Mind]] [[Desire for Spiritual States]] [[Seeker Identity]] [[Mind Tricks]]


- **The Awareness that you are, remains untouched by anything that is perceived, and every perception is just a perception**, it doesn't have the tangibility or the real sense to hurt you or touch you in any sort of way. 

So independent of whatever the perception may be, you see that you remain, untouched by any of that. 

Source: May 24th Satsang Broadcast

[[Nature of Perception]] [[Awareness]]


- The mind tries to use the discovery that Awareness is untouched, to try and control or change life, or how life operates now because we are apparently more free, we are finding ourselves. 

It tries to find evidence of that in this waking state, but this waking state is free, just like the dream, to dream whatever it has to dream. 

**So allow whatever state is coming to come up because freedom is not to have this state exactly the way my mind wants. **

Source: May 24th Satsang Broadcast

[[Desire for Spiritual States]] [[Seeker Identity]] [[Mind Tricks]] 
 
 
- **Freedom means everything can come and go.** What kind of a freedom would it be if I have to be on guard all the time.

Source: May 24th Satsang Broadcast

[[States of Body-Mind]][[Desire for Spiritual States]][[Mind Tricks]] 
 


- **Trust whatever life is bringing up and don't make any set of perceptions into a better state that we must reside in, that I have to live like this. **

[[Surrender]] [[Trust]] [[Devotion]] [[Nature of Perception]] [[Mind Tricks]]  
 


- Are we in Satsang, so that we can be 'happy' according to our mind's construct of being happy, or are we are in this (satsang)  because we are tired of the lies, and we want just what is true? 

[[Happiness]] [[Mind Tricks]]  
 
 
- 

